exports.integrantes =[
    {
      nombre:'Diego Ramirez',
      matricula:'UG0095',
      pagina:'/UG0095'
    },{
        nombre:'Gabriela Espinola', 
        matricula:'Y23715', 
        pagina:'/Y23715'
    },{
        nombre:'Zuanny Ortiz', 
        matricula:'Y26230', 
        pagina:'/Y26230'
    },{
        nombre:'Yenifer Aguilera',
        matricula:'Y12954', 
        pagina:'/Y12954'
    },{
        nombre:'Nicolas Gimenez', 
        matricula:'Y18433', 
        pagina:'/Y18433'
    }
    
];
exports.tipocontenido =[
    {nombre:'YouTube' },{nombre:'Imagen'},{nombre:'Dibujo'},
];
exports.contenido =[

    {
        nombre:'YouTube', 
        src:null, 
        url:'https://www.youtube.com/embed/210R0ozmLwg?si=bbjXLbzQS8fBaoTz', 
        matricula:'UG0095'
    },{
        nombre:'Imagen', 
        url:null, 
        src:'/Images/imagen1Diego.jpeg', 
        matricula:'UG0095'
    },{
        nombre:'Dibujo',
        url:null, 
        src:'/Images/Imagen2Diego.jpeg', 
        matricula:'UG0095'
    },{ 
        nombre:'YouTube', 
        src:null, 
        url:'https://www.youtube.com/embed/Rk7gnFCeVAY', 
        matricula:'Y23715'
    },{
        nombre:'Imagen',  
        url:null, 
        src:'/Images/Bob%20Esponja.jpeg', 
        matricula:'Y23715'
    },{
        nombre:'Dibujo',
        url:null, 
        src:'/Images/bananamichi.png', 
        matricula:'Y23715'
    },{
        nombre:'YouTube', 
        src:null, 
        url:'https://www.youtube.com/embed/Qes1RMK9a50?si=U5YTt8otRVQc1Ahk', 
        matricula:'Y26230', 
        titulo:'Video favorito de YouTube'
    },{
        nombre:'Imagen', 
        url:null, 
        src:'/Images/Extrovertida.jpg', 
        matricula:'Y26230',
        titulo:'Imagen que me representa'
    },{
        nombre:'Dibujo', 
        url: null, 
        src:'/Images/Dibujo_Zuanny.png', 
        matricula:'Y26230',
        titulo:'Mi dibujo'
    },{
        nombre:'YouTube',
        src:null, 
        url:'https://www.youtube.com/embed/ZZB5ERDc9b4?si=iDVpStmqykgB3nyV%22', 
        matricula:'Y12954'
    },{
        nombre:'Imagen', 
        url:null, 
        src:'https://img.freepik.com/vector-premium/mujer-ansiosa-traje-azul-sosteniendo-su-cabeza-estilo-dibujado-mano_317396-289.jpg', 
        matricula:'Y12954'
    },{
        nombre:'Dibujo',
        url:null, 
        src:'Imagen que Yenifer no subio', 
        matricula:'Y12954'
    },{
        nombre:'YouTube', 
        src:null, 
        url:'https://www.youtube.com/embed/YZ8j6iO0ulw?si=3qmb7GQ0CxHN2C3Q', 
        matricula:'Y18433'
    },{
        nombre:'Imagen', 
        url:null, 
        src:'/Images/mis_sueños.jpeg', 
        matricula:'Y18433'
    },{
        nombre:'Dibujo',
        url:null, 
        src:'/Images/DibujoNico.png', 
        matricula:'Y18433'
    },

];
exports.curso = [
    {
        nombre: "imagen", 
        url:null, 
        src:'/Images/uclogo.png'
    },
];
